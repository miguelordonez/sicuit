<?php
require 'vendor/autoload.php';
use \Mailjet\Resources;

//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
header('Content-type: application/json; charset=utf-8');

// If the form has been submitted with a captcha, check it - if it fails from Google, exit the script after returning an error message.
if (isset($_POST['g-recaptcha-response']) && isset($_POST['captcha'])) {

  $response = "";
  $recaptchaSecretKey = '6LfR60EUAAAAAENI4wvIlpptQWAqs3mWmk53uHUq';
  $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptchaSecretKey . "&response=" . $_POST['g-recaptcha-response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']);

  $response = json_decode($response, TRUE);

  if ($response['success'] == FALSE) {
    http_response_code(500);
    echo json_encode($recaptchaErrorMessage);
    exit;
  }
  else {
    unset($_POST['g-recaptcha-response']);
  }
}

// Send the message or catch an error if it occurs.
try{

  if(!isset($_POST['Name']) || !isset($_POST['Email']) || !isset($_POST['Message'])){
    throw new Exception('Needs mail, email and Message');
  }

  $apikey = '3fd0ebdb081a7cedc8872051af491032';
  $apisecret = 'ba3334a7ed605a1767cc1f94575aecf0';

  $mj = new \Mailjet\Client($apikey, $apisecret,true,['version' => 'v3.1']);

  $name = $_POST['Name'];
  $to = $_POST['Email'];
  $message = $_POST['Message'];

  // Mandamos a info@sicuit.es el mensaje
  $body = [
    'Messages' => [
      [
        'From' => [
          'Email' => "info@sicuit.es",
          'Name' => "Sicuit"
        ],
        'To' => [
          [
            'Email' => "info@sicuit.es"
          ]
        ],
        'Subject' => "Contacto de " . $name,
        'HTMLPart' => "<b>Nombre: </b>" . $name . "<br /><b>Email:</b>" . $to . "<br /><b>Mensaje:</b>" . $message . "<br />"
      ]
    ]
  ];
  $response = $mj->post(Resources::$Email, ['body' => $body]);

  if ($response->getStatus() != 200) {
    throw new Exception('Error sending mail');
  }

  // Mandamos mail de agradecimiento al cliente
  $body = [
    'Messages' => [
      [
        'From' => [
          'Email' => "info@sicuit.es",
          'Name' => "Sicuit"
        ],
        'To' => [
          [
            'Email' => $to
          ]
        ],
        'TemplateID' => 297712,
        'TemplateLanguage' => true
      ]
    ]
  ];
  $response = $mj->post(Resources::$Email, ['body' => $body]);

  if ($response->getStatus() != 200) {
    throw new Exception('Error sending mail');
  }
  echo json_encode($response->getStatus());
}

catch(Exception $e){
  http_response_code(500);
  echo json_encode($e->getMessage());
}

exit;

?>


