//var config = require('./gulpconfig.json');
var gulp = require('gulp');
var critical = require('critical');
var shell = require('gulp-shell');
var minifyHTML = require('gulp-minify-html');
var cloudflare = require('gulp-cloudflare');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var uncss = require('gulp-uncss');
var minifyCss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var jpegtran = require('imagemin-jpegtran');
var gifsicle = require('imagemin-gifsicle');
var replace = require('gulp-replace');
var fs = require('fs');
var download = require('gulp-download');
var run = require('gulp-run-command').default;
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cp = require('child_process');
/**
 * Build the Jekyll Site for production
 */
gulp.task('set-prod-jekyll-env', function() {
    return process.env.JEKYLL_ENV = 'production';
});
// Runs jekyll build command.
gulp.task('jekyll', run('bundle exec jekyll build --config _config.yml'));
gulp.task('jekyll-local', ['set-prod-jekyll-env'], run('bundle exec jekyll build --config _config.yml'));
// Rebuild Jekyll

gulp.task('build-jekyll', (code) => {
    return cp.spawn('jekyll', ['build', '--incremental'], { stdio: 'inherit' }) // Adding incremental reduces build time.
        .on('error', (error) => gutil.log(gutil.colors.red(error.message)))
.on('close', code);
});

gulp.task('optimize-images', function () {
    return gulp.src(['_site/assets/img/**/*.jpg', '_site/assets/img/**/*.jpeg', '_site/assets/img/**/*.gif', '_site/assets/img/**/*.png'])
        .pipe(imagemin({
            progressive: false,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant(), jpegtran(), gifsicle()]
        }))
        .pipe(gulp.dest('_site/assets/img/'));
});

gulp.task('pack-css-vendor', function () {
    return gulp.src([
        '_site/assets/css/fonts.css',
        '_site/assets/css/stack-interface.css',
        '_site/assets/css/socicon.css',
        //'_site/assets/css/lightbox.min.css',
        '_site/assets/css/flickity.css',
        '_site/assets/css/iconsmind.css',
        //'_site/assets/css/jquery.steps.css',
        '_site/assets/css/cookieconsent.min.css',
    ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('_site/assets/css'));
});

gulp.task('pack-css', function () {
    return gulp.src([
        '_site/assets/css/bootstrap.css',
        '_site/assets/css/theme.css'
    ])
        .pipe(concat('main.css'))
        .pipe(gulp.dest('_site/assets/css'));
});

gulp.task('pack-js', function () {
    return gulp.src([
        '_site/assets/js/vendor/jquery-3.1.1.min.js',
        '_site/assets/js/vendor/smooth-scroll.min.js',
        '_site/assets/js/vendor/granim.min.js',
        '_site/assets/js/vendor/jquery.steps.min.js',
        '_site/assets/js/vendor/easypiechart.min.js',
        '_site/assets/js/vendor/typed.min.js',
        '_site/assets/js/vendor/loadingoverlay.min.js',
        '_site/assets/js/vendor/flickity.min.js',
        '_site/assets/js/vendor/datepicker.js',
        '_site/assets/js/vendor/datepicker_es_ES.js',
        '_site/assets/js/scripts.js',
        '_site/assets/js/vendor/cookieconsent.min.js',
    ])
        .pipe(concat('bundle.js'))
        .pipe(minify())
        .pipe(gulp.dest('_site/assets/js'));
});
gulp.task('optimize-js', function() {
    gulp.src([
        '_site/assets/js/scripts.js',
        '_site/assets/js/vendor/cookieconsent.min.js',
        ])
        .pipe(concat('custom.js'))
        .pipe(minify())
        .pipe(gulp.dest('_site/assets/js'))
});

gulp.task('optimize-css', function() {
    return gulp.src(
        '_site/assets/css/main.css'
    )
        .pipe(autoprefixer())
        .pipe(minifyCss({keepBreaks: false}))
        .pipe(gulp.dest('_site/assets/css/'));
});
gulp.task('optimize-css-vendor', function() {
    return gulp.src(
        '_site/assets/css/vendor.css'
    )
        .pipe(autoprefixer())
        .pipe(minifyCss({keepBreaks: false}))
        .pipe(gulp.dest('_site/assets/css/'));
});

gulp.task('optimize-html', function() {
    return gulp.src('_site/**/*.html')
        .pipe(minifyHTML({
            quotes: true
        }))
        .pipe(replace(/<link href=\"\/assets\/css\/main.css\"[^>]*>/, function(s) {
            var style = fs.readFileSync('_site/assets/css/main.css', 'utf8');
            return '<style>\n' + style + '\n</style>';
        }))
        .pipe(gulp.dest('_site/'));
});
gulp.task('fetch-newest-analytics', function() {
    return download('https://www.google-analytics.com/analytics.js')
        .pipe(gulp.dest('assets/js/vendor/'));
});
/*
gulp.task('rsync-files', function() {
    return gulp.src('index.html', { read: false })
        .pipe(shell([
            'cd _site && rsync -az --delete . ' + config.remoteServer + ':' + config.remotePath
        ]));
});


gulp.task('raw-deploy', function(callback) {
    runSequence(mv _si
        'jekyll',
        'rsync-files',
        'purge-cache',
        callback
    );
});

*/

gulp.task('purge-cache', function() {
    cloudflare({
        token: '801eb29f088cdd1c13d4ef4683c5d4cfb7eea',
        email: 'mordonez.sanchez@gmail.com',
        domain: 'sicuit.es'
    });
});

gulp.task('critical', function (cb) {
    critical.generate({
        base: '_site/',
        src: 'index.html',
        css: ['_site/assets/css/main.css'],
        dimensions: [{
            width: 320,
            height: 480
        },{
            width: 768,
            height: 1024
        },{
            width: 1280,
            height: 960
        }],
        dest: '../_includes/critical.css',
        minify: true,
        extract: false,
        ignore: ['font-face']
    });
});

gulp.task('dry-run', function(callback) {
    runSequence(
        'fetch-newest-analytics',
        'jekyll-local',
        //'optimize-images',
        'pack-css',
        'pack-css-vendor',
        'pack-js',
        'optimize-js',
        'optimize-css',
        'optimize-css-vendor',
        'optimize-html',
        'critical',
        callback
    );
});

gulp.task('prod', function(callback) {
    runSequence(
        'set-prod-jekyll-env',
        'fetch-newest-analytics',
        'jekyll',
        //'optimize-images',
        'pack-css',
        'pack-css-vendor',
        'pack-js',
        'optimize-js',
        'optimize-css',
        'optimize-css-vendor',
        'optimize-html',
        'critical',
        callback
    );
});